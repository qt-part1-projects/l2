#include "valueobject.h"
#include <QFile>

#include <QVariant>

int main()
{
  // ---------------------------------------------------------------------------
  //1. Изучение наследования.
//  ValueObject o;
//  if(o.inherits("QObject")){
//      qDebug() << "ValueObject inherits QObject";
//  }
//  else
//      qDebug() << "ValueObject does not inherits QObject";

//  QFile f;
//  f.inherits("QIODevice") ? qDebug() << "QFile inherits QIODevice" : qDebug() << "QFile does not inherits QIODevice";
//  f.inherits("QDataStream") ? qDebug() << "QFile inherits QDataStream" : qDebug() << "QFile does not inherits QDataStream";
//  f.inherits("QObject") ? qDebug() << "QFile inherits QObject " : qDebug() << "QFile does not inherits QObject ";
//  f.inherits("QTemporaryFile") ? qDebug() << "QFile inherits QTemporaryFile " : qDebug() << "QFile does not inherits QTemporaryFile ";
  // ---------------------------------------------------------------------------


  // ---------------------------------------------------------------------------
  //2. Свойства
//  ValueObject o1;
//  ValueObject o2;

//  o1.setValue(1);
//  o2.setValue(2);

//  qDebug("o1: %d  o2: %d", o1.value(), o2.property("value").toInt());
//  o1.setValue(42);
//  qDebug("o1: %d  o2: %d", o1.value(), o2.property("value").toInt());
//  o2.setValue(11);
//  qDebug("o1: %d  o2: %d", o1.value(), o2.property("value").toInt());
  // ---------------------------------------------------------------------------



  // ---------------------------------------------------------------------------
  //3. Управление памятью
//  ValueObject o;
//  o.setObjectName("root");

//  ValueObject *c1 = new ValueObject(&o);
//  c1->setObjectName("child 1");
//  ValueObject *c2 = new ValueObject(&o);
//  c2->setObjectName("child 2");

//  ValueObject *c1c1 = new ValueObject(c1);
//  c1c1->setObjectName("child 1 of child 1");
//  ValueObject *c2c1 = new ValueObject(c1);
//  c2c1->setObjectName("child 2 of child 1");

//  ValueObject *c = new ValueObject();
//  c->setObjectName("child");
//  c->setParent(c2);
  // ---------------------------------------------------------------------------


  // ---------------------------------------------------------------------------
  //4. Сигналы и слоты
//  ValueObject o1;
//  ValueObject o2;
//  o1.setValue(11);
//  o2.setValue(22);
//  //QObject::connect(&o1,SIGNAL(valueChanged(int)),&o2,SLOT(setValue(int)));
//  //QObject::connect(&o2,SIGNAL(valueChanged(int)),&o1,SLOT(setValue(int)));
//  qDebug("o1: %d, o2: %d", o1.value(), o2.value());
//  o1.setValue(42);
//  qDebug("o1: %d, o2: %d", o1.value(), o2.value());
//  o2.setValue(11);
//  qDebug("o1: %d, o2: %d", o1.value(), o2.value());


  return 0;
}
