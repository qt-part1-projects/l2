#include "valueobject.h"

ValueObject::ValueObject(QObject *parent) :
    QObject(parent)
{
  m_value = 2;

  qDebug() << "ValueObject CONSTRUCTED" << this;
}

ValueObject::~ValueObject(){
    qDebug() << "ValueObject DESTRUCTED" << this;
}


void ValueObject::setValue(int v)
{
  if(v != m_value){
    m_value = v;
    qDebug("New value = %d",m_value);
    emit valueChanged(m_value);
  }
}

int ValueObject::value()
{
    return m_value;
}
