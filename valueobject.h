#ifndef VALUEOBJECT_H
#define VALUEOBJECT_H

#include <QObject>
#include <QDebug>

class ValueObject : public QObject
{
  Q_OBJECT

  //2. Свойства
  //Q_PROPERTY(int value READ value WRITE setValue)
  int m_value;

public:
    explicit ValueObject(QObject *parent = 0);
    virtual ~ValueObject();


  //2. Свойства
  int value();
  //void setValue(int v);

signals:
  //4. Сигналы и слоты
  int valueChanged(int);

public slots:
  //4. Сигналы и слоты
  void setValue(int v);
};

#endif // VALUEOBJECT_H
